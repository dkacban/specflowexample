﻿Feature: Searchbox
	In order to search for my favorite movies
	As an user
	I want to have a search box

Scenario: Search movie by correct movie title
	When I am on the main website
	And I type in 'Matrix' in searchbox
	And I press search button
	Then I should see 'The Matrix (1999)' in results

Scenario: Search movie by empty string
	When I am on the main website
	And I type in '' in searchbox
	And I press search button
	Then I should see not found information