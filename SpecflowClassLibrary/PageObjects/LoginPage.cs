﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SpecflowClassLibrary.PageObjects
{
    [Binding]
    public class LoginPage
    {
        [When(@"I am on the main website")]
        public void WhenIAmOnTheMainWebsite()
        {
            WebBrowser.Instance.WebDriver.Url = "http://www.imdb.com/";
            WebBrowser.Instance.WebDriver.Navigate();
        }

        [Then(@"I Should See logo")]
        public void ThenIShouldSeeLogo()
        {
            var element = WebBrowser.Instance.WebDriver.FindElement(By.Id("home_img_holder"));
            Assert.IsNotNull(element);
        }
    }
}
