﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SpecflowClassLibrary.PageObjects
{
    [Binding]
    public class SearchboxPage
    {
        [When(@"I type in '(.*)' in searchbox")]
        public void WhenITypeInInSearchbox(string text)
        {
            var input = WebBrowser.Instance.WebDriver.FindElement(By.Id("navbar-query"));
            input.SendKeys(text);
        }

        [When(@"I press search button")]
        public void WhenIPressSearchButton()
        {
            var element = WebBrowser.Instance.WebDriver.FindElement(By.Id("navbar-submit-button"));
            element.Click();
        }
    }
}
