﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SpecflowClassLibrary.PageObjects
{
    [Binding]
    class SearchResultsPage
    {
        [Then(@"I should see not found information")]
        public void ThenIShouldSeeNotFoundInformation()
        {
            var searchResultElement = WebBrowser.Instance.WebDriver.FindElement(By.CssSelector("#main > .article"));
            var expectedMessage = "Enter a word or phrase to search on.";

            Assert.AreEqual(expectedMessage, searchResultElement.Text);
        }

        [Then(@"I should see '(.*)' in results")]
        public void ThenIShouldSeeTextInResults(string text)
        {
            var title = WebBrowser.Instance.WebDriver.FindElement(By.CssSelector("html.scriptsOn body#styleguide-v2.fixed div#wrapper div#root.redesign div#pagecontent.pagecontent div#content-2-wide div#main div.article div.findSection table.findList tbody tr.findResult.odd td.result_text"));
            Assert.AreEqual(text, title.Text);
        }
    }
}
