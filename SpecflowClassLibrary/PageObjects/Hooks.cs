﻿using System;
using System.Drawing.Imaging;
using System.IO;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SpecflowClassLibrary.PageObjects
{
    [Binding]
    public class CommonSteps
    {
        [BeforeScenario]
        public void Before()
        {
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            WebBrowser.Instance.WebDriver.Quit();
        }

        [AfterStep]
        public void TakeScreenShot()
        {
            var screenDriver = (ITakesScreenshot)WebBrowser.Instance.WebDriver;
            var shot = screenDriver.GetScreenshot();

            var hour = DateTime.Now.Hour;
            var min = DateTime.Now.Minute;
            var sec = DateTime.Now.Second;
            var ms = DateTime.Now.Millisecond;

            var fileName = String.Format("screens/{0}_{1}_{2}_{3}.jpg", hour, min, sec, ms);

            Directory.CreateDirectory("screens");

            shot.SaveAsFile(fileName, ImageFormat.Jpeg);
        }
    }
}
