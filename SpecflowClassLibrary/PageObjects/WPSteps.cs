﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace SpecflowClassLibrary
{
    [Binding]
    public class WPPocztaSteps
    {
        [Given(@"I am on the main page")]
        public void GivenIAmOnTheMainPage()
        {
            Driver.Instance.WebDriver.Navigate().GoToUrl("http://www.wp.pl");
            Thread.Sleep(1000);
        }

        [Given(@"I click '(.*)' link")]
        public void GivenIClickLink(string p0)
        {
            var poczta = Driver.Instance.WebDriver.FindElement(By.CssSelector(".poczta > a"));
            poczta.Click();
        }

        [When(@"I type in '(.*)' login")]
        public void WhenITypeInLogin(string p0)
        {
            var login = Driver.Instance.WebDriver.FindElement(By.Id("login"));
            login.SendKeys("Specflow");
        }

        [When(@"I type in '(.*)' password")]
        public void WhenITypeInPassword(string p0)
        {
            var password = Driver.Instance.WebDriver.FindElement(By.Id("password"));
            password.SendKeys("Techtalk");
        }

        [Then(@"I should see '(.*)' button")]
        public void ThenIShouldSeeButton(string p0)
        {
            var login = Driver.Instance.WebDriver.FindElement(By.Id("buttonLogout"));
            Assert.IsNotNull(login);
        }

        [When(@"I click '(.*)' button")]
        public void WhenIClickButton(string p0)
        {
            var submit = Driver.Instance.WebDriver.FindElement(By.Id("btnSubmit"));
            submit.Click();
        }

        [Then(@"I do not see '(.*)' link")]
        public void WhenIDontSeeLink(string className)
        {
            var aukcje = Driver.Instance.WebDriver.FindElements(By.CssSelector(String.Format(".{0} > a", className)));
            Assert.AreEqual(0, aukcje.Count);
        }
    }
}
