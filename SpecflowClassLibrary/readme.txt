﻿Webdriver configurations:

Firefox:
If you can't run tests check versions.
WebDriver supports always the last stable version of firefox.
You can try to downgrade firefox version to fix potential errors.
Webdriver version -> downgrade firefox to given version:
webdriver 2.53 -> firefox 45.0

Chrome:
1. Copy chromedriver.exe from this project folder to C:/WebDrivers/
2. Add 'C:/WebDrivers/' to PATH environment variable