﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace SpecflowClassLibrary
{
    class WebBrowser
    {
        private static WebBrowser _instance;
        public IWebDriver WebDriver;

        private WebBrowser()
        {
            WebDriver = new ChromeDriver();
        }

        public static WebBrowser Instance
        {
            get { return _instance ?? (_instance = new WebBrowser()); }
        }
    }
}