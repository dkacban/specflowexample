﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;


namespace WebdriverDemo
{
    [TestClass]
    public class WebDriverTest
    {
        [TestMethod]
        public void Test()
        {
            var webDriver = new ChromeDriver {Url = "http://www.wp.pl"};
            webDriver.Navigate();
            Assert.AreEqual("Wirtualna Polska - Wszystko co ważne - www.wp.pl", webDriver.Title);
            webDriver.Close();
        }
    }
}
